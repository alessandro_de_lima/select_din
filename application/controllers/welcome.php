<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index(){
		$this->load->model("m_departamentos_produtos");

		$departamentos = $this->m_departamentos_produtos->retorna_departamentos();

		$option = "<option value=''></option>";
		foreach ($departamentos->result() as $linha) {
			$option .= "<option value='$linha->departamentos_id'>$linha->departamentos_nome</option>";
		}

		$variaveis['options_departamentos'] = $option;
 
		$this->load->view('welcome_message', $variaveis);
	}

	public function busca_produtos_by_departamento(){
		$this->load->model("m_departamentos_produtos");
 
		$produtos = $this->m_departamentos_produtos->retorna_produtos_by_departamento();
		
		$option = "<option value=''></option>";
		foreach($produtos -> result() as $linha) {
			$option .= "<option value='$linha->produtos_id'>$linha->produtos_nome</option>"; 
		}
		 
		echo $option;
	}
}